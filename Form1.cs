﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using IntelOrca.Launchpad;
using System.IO;
using System.IO.Compression;
using NAudio.Wave;
using System.Threading;
using System.Net.Http.Headers;
using System.Collections.Concurrent;
using System.Text;
using NAudio.Midi;
using Midi;
using System.Runtime.CompilerServices;
using System.Reflection;
using System.Drawing;

namespace SoundBoard
{
    public partial class Form1 : Form
    {

        record[,,] database = new record[8,8,8];

        LaunchpadDevice device;

        bool UnsavedChanges = false;

        public bool LaunchpadConnected = false;

        public Form1()
        {
            InitializeComponent();

            if (DirectSoundOut.Devices.Count().Equals(0))
            {
                MessageBox.Show("Nebylo nalezeno žádné dostupné výstupní zvukové zařízení!\n\nSpouštění programu přerušeno.", "Zvuk není dostupný", MessageBoxButtons.OK, MessageBoxIcon.Error);
                
                this.Close();
                Application.Exit();
                return;
            }


            label3.Dock = DockStyle.Top;
            label3.Visible = false;
            label3.Enabled = false;

//            tableLayoutPanel1.Invoke((MethodInvoker)delegate {



                tableLayoutPanel1.Controls.Clear();

                //tableLayoutPanel1.Controls.Clear();
                for (int y = 0; y < 8; y++)
                {
                    for (int x = 0; x < 8; x++)
                    {
                        Button btn = new Button();
                        btn.Left = 0;
                        btn.Top = 0;
                        btn.Dock = DockStyle.Fill;
                        btn.Tag = x + "," + y + ",";
                        btn.MouseUp += GuiGridBTN_MouseUp;
                        btn.Cursor = Cursors.Hand;
                        
                        btn.Text = "";
                        btn.Tag += "false";
                        tableLayoutPanel1.Controls.Add(btn, x, y);

                    }

                }
                //tableLayoutPanel1.Refresh();


            //});
        }

        public struct direcddevice
        {
            public DirectSoundDeviceInfo device { get; }
            public direcddevice(DirectSoundDeviceInfo i)
            {
                device = i;
            }
            public override string ToString()
            {
                return device.Description;
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {

                device = new LaunchpadDevice();

                LaunchpadConnected = true;

                device.DoubleBuffered = true;

                device.ButtonPressed += Launchpad_ButtonPressed;

                device.DoubleBuffered = false;

                resetLight();

            }
            catch
            {
                if (!pendingRestart)
                    switch (MessageBox.Show("Launchpad nenalezen \n\nV případě, že chcete pokračovat bez launchpadu, zvolte možnost ignorovat, některé funkce aplikace nebudou dostupné a může docházet k neočekávaným chybám.", "Error", MessageBoxButtons.AbortRetryIgnore, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1))
                    /*"Launchpad not found! \n\nIn case, that you want to continue without Launchpad, select option 'Ignore', but application will not work correctly and may throw unexpected errors!", "Error", MessageBoxButtons.AbortRetryIgnore, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1))*/
                    {
                        case DialogResult.Abort:
                            LaunchpadConnected = false;
                            this.Close();
                            break;
                        case DialogResult.Retry:
                            if (!pendingRestart)
                            {
                                pendingRestart = true;
                                LaunchpadConnected = false;
                                Application.Restart();
                            }
                            break;
                        case DialogResult.Ignore:
                            ignoreDeviceExc = true;
                            LaunchpadConnected = false;
                            break;
                        default:
                            this.Close();
                            break;
                    }
            }

            //relight();
            LoadingTKN = new CancellationTokenSource();
            if (LaunchpadConnected)
            {
                
                Task loading = new Task(() => { Loading(LoadingTKN.Token); });
                loading.Start();
            }

                foreach (var d in DirectSoundOut.Devices)
                {
                    ToolStripMenuItem i = new ToolStripMenuItem();
                    i.CheckState = CheckState.Unchecked;
                    i.Text = d.Description;
                    i.Tag = new direcddevice(d);
                    i.Click += SoundDeviceDropDownToolStrip_Click;

                    primSoundToolStrip.DropDownItems.Add(i);


                    ToolStripMenuItem it = new ToolStripMenuItem();
                    it.CheckState = CheckState.Unchecked;
                    it.Text = d.Description;
                    it.Tag = new direcddevice(d);
                    it.Click += SoundDeviceDropDownToolStrip_Click;

                    secSoundToolStrip.DropDownItems.Add(it);
                    //MicComboBox.Items.Add(new direcddevice(d));
                    //SpeakComboBox.Items.Add(new direcddevice(d));
                }
                Console.WriteLine(primSoundToolStrip.DropDownItems.Count);

                ((ToolStripMenuItem)primSoundToolStrip.DropDownItems[0]).CheckState = CheckState.Checked;
                ((ToolStripMenuItem)secSoundToolStrip.DropDownItems[0]).CheckState = CheckState.Checked;
                //MicComboBox.SelectedIndex = 0;
                //SpeakComboBox.SelectedIndex = 0;

                /*SpeakGUID = ((direcddevice)SpeakComboBox.SelectedItem).device.Guid;
                MicGUID = ((direcddevice)MicComboBox.SelectedItem).device.Guid;*/




                /*
                device[0, 0].SetBrightness(ButtonBrightness.Full, ButtonBrightness.Full);
                device[1, 0].SetBrightness(ButtonBrightness.Full, ButtonBrightness.Low);
                device[2, 0].SetBrightness(ButtonBrightness.Full, ButtonBrightness.Medium);
                device[3, 0].SetBrightness(ButtonBrightness.Full, ButtonBrightness.Off);

                device[0, 1].SetBrightness(ButtonBrightness.Low, ButtonBrightness.Full);
                device[1, 1].SetBrightness(ButtonBrightness.Low, ButtonBrightness.Low);
                device[2, 1].SetBrightness(ButtonBrightness.Low, ButtonBrightness.Medium);
                device[3, 1].SetBrightness(ButtonBrightness.Low, ButtonBrightness.Off);

                device[0, 2].SetBrightness(ButtonBrightness.Medium, ButtonBrightness.Full);
                device[1, 2].SetBrightness(ButtonBrightness.Medium, ButtonBrightness.Low);
                device[2, 2].SetBrightness(ButtonBrightness.Medium, ButtonBrightness.Medium);
                device[3, 2].SetBrightness(ButtonBrightness.Medium, ButtonBrightness.Off);

                device[0, 3].SetBrightness(ButtonBrightness.Off, ButtonBrightness.Full);
                device[1, 3].SetBrightness(ButtonBrightness.Off, ButtonBrightness.Low);
                device[2, 3].SetBrightness(ButtonBrightness.Off, ButtonBrightness.Medium);
                device[3, 3].SetBrightness(ButtonBrightness.Off, ButtonBrightness.Off);

                */

            


            this.Enabled = false;
            this.Text = "Inicializace...";

        }

        private void SoundDeviceDropDownToolStrip_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem t = (ToolStripMenuItem)sender;


            if (primSoundToolStrip.DropDownItems.Contains(t))
            {
                SpeakGUID = ((direcddevice)t.Tag).device.Guid;
                refreshDropDown(primSoundToolStrip.DropDownItems);
            } else
            {
                MicGUID = ((direcddevice)t.Tag).device.Guid;
                refreshDropDown(secSoundToolStrip.DropDownItems);
            }
            
        }

        private void refreshDropDown(ToolStripItemCollection collection)
        {

            foreach (var d in DirectSoundOut.Devices)
            {
                bool c = true;
                foreach(ToolStripMenuItem v in collection)
                {
                    if(v.Text == d.Description)
                    {
                        c = false;
                        break;
                    }
                }
                if (c)
                {
                    ToolStripMenuItem i = new ToolStripMenuItem();
                    i.CheckState = CheckState.Unchecked;
                    i.Text = d.Description;
                    i.Tag = new direcddevice(d);
                    i.Click += SoundDeviceDropDownToolStrip_Click;

                    collection.Add(i);
                }
            }
            Guid target;
            if (collection == primSoundToolStrip.DropDownItems)
            {
                target = SpeakGUID;
            } else
            {
                target = MicGUID;
            }

            foreach (ToolStripMenuItem i in collection)
            {

                if (DirectSoundOut.Devices.ToList().FindIndex(d => d.Description == i.Text) == -1)
                {
                    collection.Remove(i);
                    i.Dispose();
                }
                else
                {
                    
                    direcddevice c = (direcddevice)i.Tag;
                    if (c.device.Guid == target)
                    {
                        i.CheckState = CheckState.Checked;
                    } else
                    {
                        i.CheckState = CheckState.Unchecked;
                    }
                }
            }
            /*
            foreach (ToolStripMenuItem i in secSoundToolStrip.DropDownItems)
            {

                if (DirectSoundOut.Devices.ToList().FindIndex(d => d.Description == i.Text) == -1)
                {
                    secSoundToolStrip.DropDownItems.Remove(i);
                    i.Dispose();
                }
                else
                {
                    direcddevice c = (direcddevice)i.Tag;
                    if (c.device.Guid == SpeakGUID)
                    {
                        i.CheckState = CheckState.Checked;
                    }
                }
            }*/
        }

        DirectSoundOut Playback;
        List<DirectSoundOut> Playbacks = new List<DirectSoundOut>();
        List<DirectSoundOut> Captures = new List<DirectSoundOut>();

        CancellationTokenSource LoadingTKN;

        record lastrecord;

        bool repeat = false;

        Guid SpeakGUID;
        Guid MicGUID;

        bool PlayBoth = false;

        // need to be thread safty
        ConcurrentDictionary<LaunchpadButton, int> BFLock; // thread button locking for flash()

        List<Task> playings = new List<Task>();

        public Guid GetSpeakGUID()
        {
            return SpeakGUID;
        }

        Boolean AllowPolyphony = false;

        private void Launchpad_ButtonPressed(object sender, ButtonPressEventArgs e)
        {
            if (addNewActive && e.Type == ButtonType.Grid)
            {

                if (database[list, e.X, e.Y] != null) return;

                addnew.LaunchButtonCapture(e.X, e.Y, list);
                addNewActive = false;
                
                return;
            }
            if(EditButton[0] == -2)
            {
                if (database[list, e.X, e.Y] == null) return;
                EditButton[1] = e.Y;
                EditButton[0] = e.X;
                return;
            }
            try
            {

                if (e.Type == ButtonType.Grid)
                {
                    
                    record r = database[list, e.X, e.Y];
                    if (r == null) return;
                    PlayRecord(r, PlayBoth);

                }
                else if(e.Type == ButtonType.Side)
                {
                    switch (e.SidebarButton)
                    {
                        case SideButton.Volume:
                            // PLAY A

                            if (lastrecord == null) return;
                            PlayRecord(lastrecord, true);

                            break;
                        case SideButton.Pan:
                            // Stop B
                            StopAll();

                            break;
                        case SideButton.SoundB:
                            // Pause D
                            PauseAll();
                            
                            break;
                        case SideButton.Stop:
                            // Resume E
                            ResumeAll();

                            break;
                        case SideButton.TrackOn:
                            // Loop


                            if (repeat)
                            {
                                repeat = false;
                                device.GetButton(SideButton.TrackOn).SetBrightness(ButtonBrightness.Low, ButtonBrightness.Off);
                            }
                            else
                            {
                                repeat = true;
                                device.GetButton(SideButton.TrackOn).SetBrightness(ButtonBrightness.Low, ButtonBrightness.Low);
                            }

                            break;
                        case SideButton.Solo:
                            if (AllowPolyphony)
                            {
                                AllowPolyphony = false;
                                device.GetButton(SideButton.Solo).SetBrightness(ButtonBrightness.Full, ButtonBrightness.Off);
                            }
                            else
                            {
                                AllowPolyphony = true;
                                device.GetButton(SideButton.Solo).SetBrightness(ButtonBrightness.Off, ButtonBrightness.Full);
                            }
                            break;
                        case SideButton.Arm:
                            if (PlayBoth)
                            {
                                PlayBoth = false;
                                device.GetButton(SideButton.Arm).SetBrightness(ButtonBrightness.Medium, ButtonBrightness.Off);
                            }
                            else
                            {
                                PlayBoth = true;
                                device.GetButton(SideButton.Arm).SetBrightness(ButtonBrightness.Medium, ButtonBrightness.Full);
                            }
                            break;
                    }
                } else if(e.Type == ButtonType.Toolbar)
                {
                    if (list != (int)e.ToolbarButton)
                    {
                        resetLight();

                        list = (int)e.ToolbarButton;
                        device.GetButton(e.ToolbarButton).SetBrightness(ButtonBrightness.Full, ButtonBrightness.Full);

                        //listlabel.Text = "List: " + (list+1);
                        SetListThreadSafe(list);

                        refreshList();
                        relight();
                    }

                }
            }
            catch (DeviceException ex)
            {
                onDeviceException(ex);
            }

}

        private delegate void SafeCallDelegate(int l);
        void SetListThreadSafe(int l)
        {
            
            //if (listlabel.InvokeRequired)
            if(ToolStripList.GetCurrentParent().InvokeRequired)
            {
                var d = new SafeCallDelegate(SetListThreadSafe);
//                listlabel.Invoke(d, new object[] { l });
                ToolStripList.GetCurrentParent().Invoke(d, new object[] { l });
            }
            else
            {
                //listlabel.Text = "List: " + (l + 1);
                ToolStripList.Text = "List: " + (l + 1);
            }
            
            /*
            if (ListComboBox.GetCurrentParent().InvokeRequired)
            {
                var d = new SafeCallDelegate(SetListThreadSafe);
                //                listlabel.Invoke(d, new object[] { l });
                ListComboBox.GetCurrentParent().Invoke(d, new object[] { l });
                Console.WriteLine("invoked");
            }
            else
            {
                //listlabel.Text = "List: " + (l + 1);
                //ListComboBox.Text = "List: " + (l + 1);


                ////ListComboBox.Items.Insert(0, "List: " + (l + 1));
                SendMessage(this.ListComboBox.Control.Handle, CB_SETCUEBANNER, 1, "List: " + (l + 1));

                
                
                //ListComboBox.SelectedIndex = 0;


                //Console.WriteLine(ListComboBox.Text);
            }
            */
        }

     

        private void ResumeAll()
        {
            foreach (DirectSoundOut d in Playbacks)
            {
                if (d != null)
                    if (d.PlaybackState == PlaybackState.Paused)
                    {
                        d.Play();
                        paused = false;
                    }
            }

            foreach (DirectSoundOut d in Captures)
            {
                if (d != null)
                    if (d.PlaybackState == PlaybackState.Paused)
                    {
                        d.Play();
                        paused = false;
                    }
            }
            
        }

        bool paused = false;

        Task pausedFlash = new Task(() => {});

        private void PausedFlashFun(ref bool p, LaunchpadDevice d)
        {
            try
            {
                if (p == true)
                {
                    bool state = false;
                    while (p)
                    {

                        if (state) d.GetButton(SideButton.Stop).SetBrightness(ButtonBrightness.Off, ButtonBrightness.Off);
                        else d.GetButton(SideButton.Stop).SetBrightness(ButtonBrightness.Off, ButtonBrightness.Low);
                        
                        state = !state;
                        Thread.Sleep(350);
                    }

                    d.GetButton(SideButton.Stop).SetBrightness(ButtonBrightness.Off, ButtonBrightness.Low);


                } else
                {
                    return;
                }
            }
            catch (DeviceException ex)
            {
                onDeviceException(ex);
            }
        }

        private void PauseAll()
        {
            foreach (DirectSoundOut d in Playbacks)
            {
                if (d != null)
                    if (d.PlaybackState == PlaybackState.Playing)
                    {
                        d.Pause();
                        paused = true;
                    }
            }

            foreach (DirectSoundOut d in Captures)
            {
                if (d != null)
                    if (d.PlaybackState == PlaybackState.Playing)
                    {
                        d.Pause();
                        paused = true;
                    }
            }

            if(paused == true && pausedFlash.Status != TaskStatus.Running)
            {
                pausedFlash = new Task(() => { PausedFlashFun(ref paused, device); });
                pausedFlash.Start();
            }
        }

        private void StopAll()
        {
            repeat = false;

            foreach (DirectSoundOut d in Playbacks)
            {
                if (d != null)
                    d.Stop();
            }
            foreach (DirectSoundOut d in Captures)
            {
                if (d != null)
                    d.Stop();
            }

            foreach(CancellationTokenSource t in tokens)
            {
                try
                {
                    t.Cancel();
                }
                catch { }
            }

            paused = false;

            Playbacks.Clear();
            Captures.Clear();
            Playbacks = null;
            Captures = null;

            GC.Collect();

            Playbacks = new List<DirectSoundOut>();
            Captures = new List<DirectSoundOut>();

            playings = new List<Task>();
            relight();
        }

        private void Flash(CancellationToken token, record r, LaunchpadDevice d, ref bool LaunchpadConnected)
        {
            try {

                Button guibtn = (Button)tableLayoutPanel1.GetControlFromPosition(r.LaunchButton[0], r.LaunchButton[1]);
            int t = 350;

            if(!BFLock.ContainsKey(d[r.LaunchButton[0], r.LaunchButton[1]]))
            {
                while (!BFLock.TryAdd(d[r.LaunchButton[0], r.LaunchButton[1]], -1)) Thread.Sleep(1);
            }
            if (BFLock[d[r.LaunchButton[0], r.LaunchButton[1]]] != Thread.CurrentThread.ManagedThreadId)
            {

                while (!BFLock.TryUpdate(d[r.LaunchButton[0], r.LaunchButton[1]], Thread.CurrentThread.ManagedThreadId, -1)) Thread.Sleep(t);

            }

            int red = (int)r.red;
            int green = (int)r.green;
            while (!token.IsCancellationRequested)
            {

                if (r.list == list)
                {
                    guibtn = (Button)tableLayoutPanel1.GetControlFromPosition(r.LaunchButton[0], r.LaunchButton[1]);
                    if (red > 2)
                    {
                        green = 3;
                        red = 0;
                    }
                    else
                    {
                        green = 0;
                        red = 3;
                    }
                    //Console.WriteLine(Thread.CurrentThread.ManagedThreadId + " | " + r.Name);
                    if(LaunchpadConnected) d[r.LaunchButton[0], r.LaunchButton[1]].SetBrightness((ButtonBrightness)red, (ButtonBrightness)green);


                        guibtn.BackColor = Color.FromArgb(100, 30 * red, 30 * green, 0);

                    Thread.Sleep(t);
                }
            }

            // pokud po následující dobu t/2 si tlačítko nezamkne jiný task, změní se barva tl. na barvu definovanou recordem
            while (!BFLock.TryUpdate(d[r.LaunchButton[0], r.LaunchButton[1]], -1, BFLock[d[r.LaunchButton[0], r.LaunchButton[1]]])) Thread.Sleep(1);

            Thread.Sleep(t/2);

            if (r.list == list && BFLock[d[r.LaunchButton[0], r.LaunchButton[1]]] == -1 && LaunchpadConnected) d[r.LaunchButton[0], r.LaunchButton[1]].SetBrightness(r.red, r.green);
                guibtn.BackColor = SystemColors.Control;
                guibtn.UseVisualStyleBackColor = true;
                
                
            }
            catch (DeviceException ex)
            {
                onDeviceException(ex);
            }
        }

        private void OnPlaybackStopped(object sender, StoppedEventArgs args, CancellationTokenSource tkn, int pos, AudioFileReader reader, bool rep)
        {
            try
            {

                if (rep)
                {
                    reader.Position = 0;
                    if (pos > -1) Playbacks[pos].Play();
                    return;
                }

                reader.Close();
                tkn.Cancel();
                if (pos > -1)
                {
                    Playbacks[pos].Dispose();
                    Playbacks[pos] = null;
                }
            }
            catch { }
        }
        private void OnCaptureStopped(object sender, StoppedEventArgs args, int pos, AudioFileReader reader, bool rep)
        {
            try
            {
                if (rep)
                {
                    reader.Position = 0;
                    if (pos > -1) Captures[pos].Play();
                    return;
                }

                reader.Close();
                if (pos > -1)
                {
                    Captures[pos].Dispose();
                    Captures[pos] = null;
                }
            }
            catch { }
        }

        List<CancellationTokenSource> tokens = new List<CancellationTokenSource>();

        private void PlayRecord(record r, bool pb /* PlayBoth */)
        {
            lastrecord = r;

            if (!AllowPolyphony)
            {
                foreach (DirectSoundOut d in Playbacks)
                {
                    if (d != null)
                        if (d.PlaybackState != PlaybackState.Stopped)
                        {
                            return;
                        }
                }
            }

            DirectSoundOut p = new DirectSoundOut(SpeakGUID);
            AudioFileReader a = new AudioFileReader(r.FileName);



            a.Volume = r.Volume / 100.0f;

            p.Init(a);

            CancellationTokenSource tkn = new CancellationTokenSource();
            if (LaunchpadConnected) { 
                Task playing = new Task(() => { Flash(tkn.Token, r, device, ref LaunchpadConnected); });



                playing.Start();
                playings.Add(playing);
            }

            tokens.Add(tkn);

            Playbacks.Add(p);

            // nelze předat objekt repeat, protože při jeho změně by se hodnota propsala i do funkce OnPlaybackStopped - přestože se nepředává reference
            if(repeat)
                Playbacks[Playbacks.IndexOf(p)].PlaybackStopped += (senderr, ee) => OnPlaybackStopped(senderr, ee, tkn, Playbacks.IndexOf(p), a, true);
            else
                Playbacks[Playbacks.IndexOf(p)].PlaybackStopped += (senderr, ee) => OnPlaybackStopped(senderr, ee, tkn, Playbacks.IndexOf(p), a, false);

            if (pb)
            {
                
                DirectSoundOut c = new DirectSoundOut(MicGUID);
                AudioFileReader b = new AudioFileReader(r.FileName);

                b.Volume = r.Volume / 100.0f;

                c.Init(b);


                Captures.Add(c);
                Captures[Captures.IndexOf(c)].PlaybackStopped += (senderr, ee) => OnCaptureStopped(senderr, ee, Captures.IndexOf(c), b, repeat);
                Captures[Captures.IndexOf(c)].Play();

            }

            Playbacks[Playbacks.IndexOf(p)].Play(); //přesunuto sem pro snahu co nejvyšší souběžnosti

        }

        private delegate void ThreadSafeDisposeDelegate(System.Windows.Forms.Control o);
        void ThreadSafeDisposeControl(System.Windows.Forms.Control o)
        {
            //if (listlabel.InvokeRequired)
            if (ToolStripList.GetCurrentParent().InvokeRequired)
            {
                var d = new ThreadSafeDisposeDelegate(ThreadSafeDisposeControl);
                o.Invoke(d, new object[] { o });
            }
            else
            {
                o.Dispose();
            }
        }

        void switchToMainThread(int i)
        {
            //            if (listlabel.InvokeRequired)
            
            if (ToolStripList.GetCurrentParent().InvokeRequired)
            {
                var d = new SafeCallDelegate(switchToMainThread);
                ToolStripList.GetCurrentParent().Invoke(d, new object[] { 0 });
            }
        }


        private void refreshList()
        {
            try
            {
                /*
                listBox1.Items.Clear();
                foreach (record r in database)
                {
                    if (r != null && r.list == list)
                    {
                        listBox1.Items.Add(r);
                        //device[r.LaunchButton[0], r.LaunchButton[1]].SetBrightness(r.red, r.green);

                    }
                }*/


                /*
                Button btn = new Button();
                btn.Text = "test";
                btn.Left = 0;
                btn.Top = 0;
                btn.Width = 100;
                btn.Height = 30;
                btn.Dock = DockStyle.Fill;

                tableLayoutPanel1.Controls.Add(btn, 4,4);*/

                


                tableLayoutPanel1.Invoke((MethodInvoker)delegate {


                    for (int y = 0; y < 8; y++)
                    {
                        for (int x = 0; x < 8; x++)
                        {
                            record rec = database[list, x, y];
                            Button btn = (Button)tableLayoutPanel1.GetControlFromPosition(x, y);
                            //btn.Left = 0;
                            //btn.Top = 0;
                            //btn.Dock = DockStyle.Fill;
                            btn.Tag = x + "," + y + ",";
                            //btn.Click += GuiGridBTN_Click;

                            if (btn.BackColor != SystemColors.Control)
                            {
                                btn.BackColor = SystemColors.Control;
                                btn.UseVisualStyleBackColor = true;
                            }

                            if (rec != null)
                            {
                                //btn.BackColor = Color.FromArgb(150, 30 * (int)rec.red, 30 * (int)rec.green, 0);

                                btn.Text = rec.Name;
                                btn.Tag += "true";
                            } else
                            {
                                btn.Text = "";
                                btn.Tag += "false";
                            }
                            //tableLayoutPanel1.Controls.Add(btn, x, y);

                        }

                    }
                    //tableLayoutPanel1.Refresh();
                
                    
                 });


            }
            catch
            {

            }
        }

        private void resetLight()
        {
            if (!LaunchpadConnected) return;
            try { 
            foreach(LaunchpadButton b in device.Buttons)
            {
                b.SetBrightness(ButtonBrightness.Off, ButtonBrightness.Off);
                    //Thread.Sleep(10);
            }

            device.GetButton(ToolbarButton.Up).SetBrightness(ButtonBrightness.Off, ButtonBrightness.Off);
            device.GetButton(ToolbarButton.Down).SetBrightness(ButtonBrightness.Off, ButtonBrightness.Off);
            device.GetButton(ToolbarButton.Left).SetBrightness(ButtonBrightness.Off, ButtonBrightness.Off);
            device.GetButton(ToolbarButton.Right).SetBrightness(ButtonBrightness.Off, ButtonBrightness.Off);
            device.GetButton(ToolbarButton.Session).SetBrightness(ButtonBrightness.Off, ButtonBrightness.Off);
            device.GetButton(ToolbarButton.User1).SetBrightness(ButtonBrightness.Off, ButtonBrightness.Off);
            device.GetButton(ToolbarButton.User2).SetBrightness(ButtonBrightness.Off, ButtonBrightness.Off);
            device.GetButton(ToolbarButton.Mixer).SetBrightness(ButtonBrightness.Off, ButtonBrightness.Off);
            device.GetButton(ToolbarButton.Up).SetBrightness(ButtonBrightness.Off, ButtonBrightness.Off);
            }
            catch (DeviceException ex)
            {
                onDeviceException(ex);
                //return;
            }
        }

        public bool addNewActive = false;
        addForm addnew;

        private void AddBTN_Click(object sender, EventArgs e)
        {
            //if (!addnew.IsDisposed) return;
            addnew = new addForm(this, list);
            //addNewActive = true;
            addnew.Show();
        }

        int list = 0;
        public void AddInterface(record r)
        {
            database[r.list, r.LaunchButton[0], r.LaunchButton[1]] = r;

            //listBox1.Items.Add(r);

            UnsavedChanges = true;

            this.Show();

            addnew.Close();
            addNewActive = false;
            relight();
            refreshList();
        }
        public void AddNewCancel(bool i = false)
        {
            this.Show();
            if (!i) addnew.Close();
            addNewActive = false;
            resetLight();
            relight();
        }

        private void relight()
        {
            if (!LaunchpadConnected) return;
            try { 
            // Light up control buttons
            device.GetButton(SideButton.Volume).SetBrightness(ButtonBrightness.Off, ButtonBrightness.Full); // play A
            device.GetButton(SideButton.Pan).SetBrightness(ButtonBrightness.Medium, ButtonBrightness.Off); // stop B

            device.GetButton(SideButton.SoundB).SetBrightness(ButtonBrightness.Medium, ButtonBrightness.Medium); // pause D
            device.GetButton(SideButton.Stop).SetBrightness(ButtonBrightness.Off, ButtonBrightness.Low); // Resume E

            //device.GetButton(SideButton.Solo).SetBrightness(ButtonBrightness.Off, ButtonBrightness.Medium); // Play async

            if (repeat)
                device.GetButton(SideButton.TrackOn).SetBrightness(ButtonBrightness.Low, ButtonBrightness.Low);
            else
                device.GetButton(SideButton.TrackOn).SetBrightness(ButtonBrightness.Low, ButtonBrightness.Off);


            if (AllowPolyphony)                                                                                 // switch polyphony
                device.GetButton(SideButton.Solo).SetBrightness(ButtonBrightness.Off, ButtonBrightness.Full);
            else
                device.GetButton(SideButton.Solo).SetBrightness(ButtonBrightness.Full, ButtonBrightness.Off);

            //device.GetButton(SideButton.Arm).SetBrightness(ButtonBrightness.Medium, ButtonBrightness.Off); // Switch play both H

            if (PlayBoth) device.GetButton(SideButton.Arm).SetBrightness(ButtonBrightness.Medium, ButtonBrightness.Full); //Switch play both H
            else device.GetButton(SideButton.Arm).SetBrightness(ButtonBrightness.Medium, ButtonBrightness.Off);

            try
            {

                foreach (record r in database)
                {
                    if (r != null && r.list == list)
                    {
                        device[r.LaunchButton[0], r.LaunchButton[1]].SetBrightness(r.red, r.green);
                    }
                }
            }
            catch
            {

            }
            device.GetButton((ToolbarButton)list).SetBrightness(ButtonBrightness.Full, ButtonBrightness.Full); // active list toolbar button

                /*
                switch (list)
                {
                    case 0:
                        device.GetButton(ToolbarButton.Up).SetBrightness(ButtonBrightness.Full, ButtonBrightness.Full);
                        break;
                    case 1:
                        device.GetButton(ToolbarButton.Down).SetBrightness(ButtonBrightness.Full, ButtonBrightness.Full);
                        break;
                    case 2:
                        device.GetButton(ToolbarButton.Left).SetBrightness(ButtonBrightness.Full, ButtonBrightness.Full);
                        break;
                    case 3:
                        device.GetButton(ToolbarButton.Right).SetBrightness(ButtonBrightness.Full, ButtonBrightness.Full);
                        break;
                    case 4:
                        device.GetButton(ToolbarButton.Session).SetBrightness(ButtonBrightness.Full, ButtonBrightness.Full);
                        break;
                    case 5:
                        device.GetButton(ToolbarButton.User1).SetBrightness(ButtonBrightness.Full, ButtonBrightness.Full);
                        break;
                    case 6:
                        device.GetButton(ToolbarButton.User2).SetBrightness(ButtonBrightness.Full, ButtonBrightness.Full);
                        break;
                    case 7:
                        device.GetButton(ToolbarButton.Mixer).SetBrightness(ButtonBrightness.Full, ButtonBrightness.Full);
                        break;
                    default:
                        list = 0;
                        device.GetButton(ToolbarButton.Up).SetBrightness(ButtonBrightness.Full, ButtonBrightness.Full);
                        device.GetButton(ToolbarButton.Up).TurnOnLight();
                        break;
                }*/
            }
            catch (DeviceException ex)
            {
                onDeviceException(ex);
            }
        }

        private void LoadData(string file)
        {
            StopAll();

            string tmp = this.Text;
            this.Text = "Načítání konfigurace...";
            this.Enabled = false;


            string subPath = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "LaunchpadSoundBoard\\WorkingConfiguration");
            if (!System.IO.Directory.Exists(subPath)) System.IO.Directory.CreateDirectory(subPath);

            using (ZipArchive archive = ZipFile.Open(file, ZipArchiveMode.Read, Encoding.UTF8))
            {
                double fin = (double)archive.Entries.Count / 100;
                int c = 0;

                foreach(ZipArchiveEntry entry in archive.Entries)
                {
                    entry.ExtractToFile(subPath + "\\" + entry.Name, true);

                    c++;
                    this.Text = "Načítání konfigurace: Extrahování... (" + (int)(c/fin) + "%)";
                }
            }

            using (BinaryReader br = new BinaryReader(File.Open(subPath + "\\data.sbdb", FileMode.Open)))
            {
                try
                {
                    int l = br.ReadInt32();
                    double fin = (double)(l+1) / 100;
                    int count = 0;
                    for (int i = 0; i < l; i++)
                    {
                        string name = br.ReadString();

                        string fileName = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "LaunchpadSoundBoard\\WorkingConfiguration\\" + br.ReadString());

                        //Console.WriteLine(fileName);

                        int volume = br.ReadInt32();
                        int btnX = br.ReadInt32();
                        int btnY = br.ReadInt32();
                        string green = br.ReadString();
                        string red = br.ReadString();
                        int Llist = br.ReadInt32();


                        int[] buttons = new int[2];
                        buttons[0] = btnX;
                        buttons[1] = btnY;

                        record r = new record(fileName, volume, name, Llist, buttons);

                        switch (green)
                        {
                            case "Off":
                                r.green = ButtonBrightness.Off;
                                break;
                            case "Low":
                                r.green = ButtonBrightness.Low;
                                break;
                            case "Medium":
                                r.green = ButtonBrightness.Medium;
                                break;
                            case "Full":
                                r.green = ButtonBrightness.Full;
                                break;
                            default:
                                r.green = ButtonBrightness.Full;
                                break;
                        }
                        switch (red)
                        {
                            case "Off":
                                r.red = ButtonBrightness.Off;
                                break;
                            case "Low":
                                r.red = ButtonBrightness.Low;
                                break;
                            case "Medium":
                                r.red = ButtonBrightness.Medium;
                                break;
                            case "Full":
                                r.red = ButtonBrightness.Full;
                                break;
                            default:
                                r.red = ButtonBrightness.Full;
                                break;
                        }

                        database[Llist, btnX, btnY] = r;

                        count++;
                        this.Text = "Načítání konfigurace: Načítání... (" + (int)(count / fin) + "%)";

                    }

                    SpeakGUID = new Guid(br.ReadString());
                    MicGUID = new Guid(br.ReadString());

                    //MicComboBox.Enabled = true;
                    //SpeakComboBox.Enabled = true;
                    /*
                    foreach (var d in DirectSoundOut.Devices)
                    {
                        if (d.Guid == MicGUID)
                        {
                            MicComboBox.SelectedItem = new direcddevice(d);
                            //MicComboBox.Text = d.ModuleName;
                        }
                        if (d.Guid == SpeakGUID)
                        {
                            foreach(direcddevice c in SpeakComboBox.Items)
                            {
                                if (c.device.Guid == SpeakGUID) SpeakComboBox.SelectedItem = c;
                            }
                             
                            
                            //SpeakComboBox.Text = d.ModuleName;
                        }
                    }*/
                    //foreach (direcddevice c in SpeakComboBox.Items)


                    refreshDropDown(primSoundToolStrip.DropDownItems);
                    refreshDropDown(secSoundToolStrip.DropDownItems);

                    /*foreach (direcddevice c in MicComboBox.Items)
                    {
                        if (c.device.Guid == MicGUID) MicComboBox.SelectedItem = c;
                    }*/

                    count++;
                    this.Text = "Načítání konfigurace: Načítání... (" + (int)(count / fin) + "%)";

                    //MicComboBox.Update();
                    //SpeakComboBox.Update();

                    //MicComboBox.Enabled = false;
                    //SpeakComboBox.Enabled = false;
                    //UnlockBTN.Enabled = true;
                    br.Close();

                    this.Text = tmp;
                    this.Enabled = true;

                    UnsavedChanges = false;
                } catch (Exception e) {
                    Console.WriteLine(e.Message);
                }
            }
            relight();
            refreshList();
        }

        public void setColor(int x, int y, ButtonBrightness red, ButtonBrightness green)
        {
            //resetLight();
            //relight();
            try
            {
                if(LaunchpadConnected)
                    device[x, y].SetBrightness(red, green);
            } catch (Midi.DeviceException e)
            {
                onDeviceException(e);
                //MessageBox.Show(":(\nAj, někde se vyskytla chyba.\nJe launchpad připojen?\n"+e.Message, "Device Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void SaveBTN_Click(object sender, EventArgs e)
        {
            SaveData();
        }

        public int getList()
        {
            return this.list;
        }

        private void SaveData()
        {
            StopAll();

            string subPath = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "LaunchpadSoundBoard");

            if (!System.IO.Directory.Exists(subPath)) System.IO.Directory.CreateDirectory(subPath);



            SaveFileDialog dialog = new SaveFileDialog();
            dialog.Filter = "SoundBoard Configuration File (*.sbcf) | *.sbcf";
            dialog.InitialDirectory = subPath;
            dialog.FileName = "Configuration.sbcf";
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                using (BinaryWriter bw = new BinaryWriter(File.Open(subPath + "\\data.sbdb", FileMode.Create)))
                {

                    try
                    {
                        string tmp = this.Text;
                        this.Text = "Ukládání... (0%)";
                        this.Enabled = false;

                        int length = 0;
                        foreach (record rec in database)
                        {
                            if (rec != null && rec.FileName.Trim() != "" && rec.FileName.Trim() != null)
                            {
                                length++;
                            }
                        }

                        bw.Write(length);

                        List<string> SoundFiles = new List<string>(database.Length);

                        int count = 0;
                        double fin = (double)((length * 2) + 1)/100;

                        /* huh?
                        * 
                        * int length = 64;
                        * double fin = ((length * 2) + 1)/100; -> dělení se provede jako int
                        * => fin = 1
                        * 
                        * int length = 64;
                        * double fin = (double)((length * 2) + 1)/100; -> dělení se provede jako double
                        * => fin = 1.29
                        * 
                        */

                        foreach (record r in database)
                        {
                            if (r != null)
                            {
                                bw.Write(r.Name);

                                string[] fn = r.FileName.Split('\\');
                                // Console.WriteLine(fn.Last().ToString());
                                SoundFiles.Add(r.FileName);

                                bw.Write(fn.Last());
                                
                                bw.Write(r.Volume);
                                bw.Write(r.LaunchButton[0]);
                                bw.Write(r.LaunchButton[1]);
                                bw.Write(r.green.ToString());
                                bw.Write(r.red.ToString());
                                bw.Write(r.list);

                                count++;
                                this.Text = "Ukládání... (" + (int)(count / fin) + "%)";

                            }

                        }
                        bw.Write(SpeakGUID.ToString());
                        bw.Write(MicGUID.ToString());
                        bw.Close();

                        Console.WriteLine(count);

                        count++;
                        this.Text = "Ukládání... (" + (int)(count / fin) + "%)";

                        if (File.Exists(dialog.FileName))
                        {
                            File.Delete(dialog.FileName);
                        }

                        using (ZipArchive archive = ZipFile.Open(dialog.FileName, ZipArchiveMode.Create, Encoding.UTF8))
                        {
                            archive.CreateEntryFromFile(subPath + "\\data.sbdb", "data.sbdb");

                            foreach(string f in SoundFiles)
                            {
                                archive.CreateEntryFromFile(f, f.Split('\\').Last(), CompressionLevel.Optimal);

                                //File.Delete(f);

                                count++;
                                this.Text = "Ukládání... (" + (int)(count / fin) + "%)";
                            }
                        }

                        if (File.Exists(subPath + "\\data.sbdb"))
                        {
                            File.Delete(subPath + "\\data.sbdb");
                        }

                        this.Enabled = true;
                        this.Text = tmp;

                        UnsavedChanges = false;
                    }
                    catch (Exception e) {
                        Console.WriteLine(e.Message);
                    }

                }
            }
        }

        private void GuiGridBTN_Right_Click(Button btn, EventArgs e, string[] d)
        {

            if (d[2] != "false")
            {
                if(MessageBox.Show("Opravdu si přejete záznam odstranit?", "Smazat", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    database[list, Convert.ToInt32(d[0]), Convert.ToInt32(d[1])] = null;
                    UnsavedChanges = true;
                    resetLight();
                    refreshList();
                    relight();
                }

            }

        }

        private void GuiGridBTN_MouseUp(object sender, MouseEventArgs e)
        {

            Button btn = (Button)sender;

            string[] d = btn.Tag.ToString().Split(',');

            // kontrola, zda je cursor stále na tlačítku
            if (!btn.ClientRectangle.Contains(e.Location))
            {
                return;
            }


            // right click redirect
            if (e.Button == MouseButtons.Right)
            {
                GuiGridBTN_Right_Click(btn, e, d);
                return;
            }
            

            if(UIPlayMode)
            {

                if(d[2] != "false") Launchpad_ButtonPressed(this, new ButtonPressEventArgs(Convert.ToInt32(d[0]), Convert.ToInt32(d[1])));
                
                return;
            }

            if (d[2] == "false")
            {
                
                AddBTN_Click(sender, e);
                
                addnew.LaunchButtonCapture(Convert.ToInt32(d[0]), Convert.ToInt32(d[1]), list);
                
            } else
            {

                record rec = database[list, Convert.ToInt32(d[0]), Convert.ToInt32(d[1])];
                addnew = new addForm(this, rec.list);

                addnew.LoadEditData(rec);
                addnew.LaunchButtonCapture(Convert.ToInt32(d[0]), Convert.ToInt32(d[1]), list);

                addnew.ShowDialog();

            }

        }

        private void LoadBTN_Click(object sender, EventArgs e)
        {
            CheckUnsaved();

            string subPath = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "LaunchpadSoundBoard");

            if (!System.IO.Directory.Exists(subPath)) System.IO.Directory.CreateDirectory(subPath);

            //Console.WriteLine(subPath);

            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "SoundBoard Configuration File (*.sbcf) | *.sbcf";
            dialog.InitialDirectory = "";
            dialog.FileName = "Configuration.sbcf";
            dialog.ShowHelp = true;
            DialogResult result = dialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                database = new record[8, 8, 8];
                LoadData(dialog.FileName);
            } else if(result != DialogResult.Cancel)
            {
                MessageBox.Show("Soubor nevybrán.", "File error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            CheckUnsaved();
            try
            {
                if (device != null)
                {
                    Playback.Stop();
                    Playback.Dispose();

                    foreach (DirectSoundOut d in Playbacks)
                    {
                        if(d != null)
                        if (d.PlaybackState == PlaybackState.Paused)
                        {
                            d.Stop();
                        }
                    }
                    foreach (DirectSoundOut d in Captures)
                    {
                        if (d != null)
                        if (d.PlaybackState == PlaybackState.Paused)
                        {
                            d.Stop();
                        }
                    }

                    Playbacks.Clear();
                    Captures.Clear();

                    device.Reset();
                    device = null;
                }

                System.IO.Directory.Delete(System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "LaunchpadSoundBoard\\WorkingConfiguration"), true);

            } catch { }
        }

        private void CheckUnsaved()
        {
            if (UnsavedChanges)
            {
                if(MessageBox.Show("Nalezeny nauložené změny!\nPřejete si je uložit?", "Varování", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    SaveData();
                }
            }
        }

        private void comboBoxMIC_SelectedIndexChanged(object sender, EventArgs e)
        {
            //MicGUID = ((direcddevice)MicComboBox.SelectedItem).device.Guid;
        }

        private void comboBoxSPEAK_SelectedIndexChanged(object sender, EventArgs e)
        {
           // SpeakGUID = ((direcddevice)SpeakComboBox.SelectedItem).device.Guid;
        }

        private void UnlockBTN_Click(object sender, EventArgs e)
        {
            //MicComboBox.Enabled = true;
            //SpeakComboBox.Enabled = true;
        }

        

        private void Form1_Shown(object sender, EventArgs e)
        {
            string subPath = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "LaunchpadSoundBoard");
            if (File.Exists(subPath + "\\Configuration.sbcf"))
            {
                LoadData(subPath + "\\Configuration.sbcf");
            }

            BFLock = new ConcurrentDictionary<LaunchpadButton, int>(Environment.ProcessorCount * 2, database.Length + 1);

            Playback = new DirectSoundOut(SpeakGUID);

            LoadingTKN.Cancel();
            relight();

            this.Text = "Launchpad SoundBoard";
            this.Enabled = true;
        }

        private void Loading(CancellationToken token)
        {
            try { 
            LaunchpadButton prev = device[0,0];
            while(!token.IsCancellationRequested)
                foreach(LaunchpadButton b in device.Buttons)
                {
                    if (token.IsCancellationRequested) return;

                    prev.TurnOffLight();

                    prev = b;

                    b.TurnOnLight();

                    b.SetBrightness(ButtonBrightness.Full, ButtonBrightness.Full);

                    Thread.Sleep(80);

                }
            }
            catch (DeviceException ex)
            {
                onDeviceException(ex);
            }
        }

        bool pendingRestart = false;

        bool ignoreDeviceExc = false;

        public void onDeviceException(DeviceException e)
        {
            LaunchpadConnected = false;
            if (ignoreDeviceExc) return;
            if (
                MessageBox.Show(
                    "Zařízení bylo odpojeno.\n" +
                    "Přejete si program restartovat?", 
                    "Device Error", 
                    MessageBoxButtons.YesNo, 
                    MessageBoxIcon.Error
                ) == DialogResult.Yes)
            {
                if (!pendingRestart)
                {
                    pendingRestart = true;
                    Application.Restart();
                }
                
            } else
            {
                ignoreDeviceExc = true;
            }
        }

        int[] EditButton = new int[2];

        private void EditBTN_Click(object sender, EventArgs e)
        {
            

            label3.Visible = true;
            label3.Enabled = true;

            /*toolStrip1.Visible = false;
            toolStrip1.Enabled = false;

            toolStrip2.Visible = false;
            toolStrip2.Enabled = false;*/

            this.Focus();
            this.Select();

            this.Enabled = false;
            
            //label3.Dock = DockStyle.Fill
            EditButton[0] = -2;

            while (EditButton[0] == -2) Application.DoEvents();

            this.Enabled = true;

            label3.Visible = false;
            label3.Enabled = false;

            toolStrip1.Visible = true;
            toolStrip1.Enabled = true;

            toolStrip2.Visible = true;
            toolStrip2.Enabled = true;

            if (EditButton[0] == -1) return;
            

            record rec = database[list, EditButton[0], EditButton[1]];

            EditButton[0] = -1;

            addnew = new addForm(this, rec.list);
            //addNewActive = true;

            addnew.LoadEditData(rec/*rec.Name, rec.FileName, rec.red, rec.green, rec.LaunchButton, rec.Volume, list*/);

            addnew.ShowDialog();

        }


        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Escape && EditButton[0] == -2)
            {
                EditButton[0] = -1;
            }
        }

        private void comboBoxMIC_DropDown(object sender, EventArgs e)
        {
            /*
            MicComboBox.Items.Clear();
            foreach (var d in DirectSoundOut.Devices)
            {
                MicComboBox.Items.Add(new direcddevice(d));

                if(d.Guid == MicGUID)
                MicComboBox.SelectedItem = new direcddevice(d);
            }

            */
        }

        
        private void comboBoxSPEAK_DropDown(object sender, EventArgs e)
        {
            /*
            SpeakComboBox.Items.Clear();
            foreach (var d in DirectSoundOut.Devices)
            {
                SpeakComboBox.Items.Add(new direcddevice(d));

                if (d.Guid == SpeakGUID)
                    SpeakComboBox.SelectedItem = new direcddevice(d);
            }*/
        }

        private void splitContainer1_Panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void SoundDeviceEdit_Click(object sender, EventArgs e)
        {
            SoundDeviceDialog dialog = new SoundDeviceDialog(this, SpeakGUID, MicGUID);
            dialog.ShowDialog();
        }


        public void SetSoundDevice(Guid speak, Guid mic)
        {
            SpeakGUID = speak;
            MicGUID = mic;
        }
        public Guid[] GetSoundDevice()
        {
            Guid[] g =  { SpeakGUID, MicGUID };
            return g;
        }

        private void primSoundToolStrip_DropDownOpening(object sender, EventArgs e)
        {
            refreshDropDown(primSoundToolStrip.DropDownItems);
        }

        private void secSoundToolStrip_DropDownOpening(object sender, EventArgs e)
        {
            refreshDropDown(secSoundToolStrip.DropDownItems);
        }


        bool UIPlayMode = false;

        private void PlayEditModeButton_Click(object sender, EventArgs e)
        {
            UIPlayMode = !UIPlayMode;
            if (UIPlayMode)
            {
                PlayEditModeButton.Text = "Režim přehrávání";
            } else
            {
                PlayEditModeButton.Text = "Režim úprav";
            }

            
            

        }

        private void ToolStripList_Click(object sender, EventArgs e)
        {
            if (ModifierKeys == Keys.Shift)
            {

                list--;

            }else
            {
                list++;
            }



                
            if (list > 7) list = 0;
            if (list < 0) list = 7;


            resetLight();

            if(!ignoreDeviceExc) device.GetButton((ToolbarButton)list).SetBrightness(ButtonBrightness.Full, ButtonBrightness.Full);

            //listlabel.Text = "List: " + (list+1);
            SetListThreadSafe(list);

            refreshList();
            relight();

        }


        /*
        private void ListComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (list != ListComboBox.SelectedIndex && ListComboBox.SelectedIndex != 0)
            {
                resetLight();

                list = (int)ListComboBox.SelectedIndex;
                device.GetButton((ToolbarButton)ListComboBox.SelectedIndex).SetBrightness(ButtonBrightness.Full, ButtonBrightness.Full);
               

                //listlabel.Text = "List: " + (list+1);
                SetListThreadSafe(list);

                refreshList();
                relight();
            }
        }
        */
    }

}
