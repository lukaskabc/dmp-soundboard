﻿namespace SoundBoard
{
    partial class addForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(addForm));
            this.label1 = new System.Windows.Forms.Label();
            this.FileTextBox = new System.Windows.Forms.TextBox();
            this.VolumeLabel = new System.Windows.Forms.Label();
            this.VolumeTrackBall = new System.Windows.Forms.TrackBar();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.LaunchBTNtextBox = new System.Windows.Forms.TextBox();
            this.PlayBTN = new System.Windows.Forms.Button();
            this.DoneBTN = new System.Windows.Forms.Button();
            this.CancelBTN = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.NameTextBox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.RedTrack = new System.Windows.Forms.TrackBar();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.GreenTrack = new System.Windows.Forms.TrackBar();
            this.ListNumericUpDown = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.VolumeTrackBall)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RedTrack)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GreenTrack)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ListNumericUpDown)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 68);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(84, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "MP3 soubor";
            // 
            // FileTextBox
            // 
            this.FileTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.FileTextBox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.FileTextBox.Location = new System.Drawing.Point(20, 87);
            this.FileTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.FileTextBox.Name = "FileTextBox";
            this.FileTextBox.ReadOnly = true;
            this.FileTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.FileTextBox.Size = new System.Drawing.Size(345, 22);
            this.FileTextBox.TabIndex = 3;
            this.FileTextBox.Text = "Vybrat";
            this.FileTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.FileTextBox.Click += new System.EventHandler(this.FileTextBox_Click);
            // 
            // VolumeLabel
            // 
            this.VolumeLabel.AutoSize = true;
            this.VolumeLabel.Location = new System.Drawing.Point(16, 130);
            this.VolumeLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.VolumeLabel.Name = "VolumeLabel";
            this.VolumeLabel.Size = new System.Drawing.Size(62, 17);
            this.VolumeLabel.TabIndex = 4;
            this.VolumeLabel.Text = "Hlasitost";
            // 
            // VolumeTrackBall
            // 
            this.VolumeTrackBall.Location = new System.Drawing.Point(16, 150);
            this.VolumeTrackBall.Margin = new System.Windows.Forms.Padding(4);
            this.VolumeTrackBall.Maximum = 100;
            this.VolumeTrackBall.Minimum = 1;
            this.VolumeTrackBall.Name = "VolumeTrackBall";
            this.VolumeTrackBall.Size = new System.Drawing.Size(349, 56);
            this.VolumeTrackBall.TabIndex = 5;
            this.VolumeTrackBall.Value = 100;
            this.VolumeTrackBall.Scroll += new System.EventHandler(this.VolumeTrackBall_Scroll);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 185);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(28, 17);
            this.label3.TabIndex = 6;
            this.label3.Text = "0%";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(328, 185);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(44, 17);
            this.label4.TabIndex = 7;
            this.label4.Text = "100%";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(16, 220);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(96, 17);
            this.label5.TabIndex = 8;
            this.label5.Text = "LaunchButton";
            // 
            // LaunchBTNtextBox
            // 
            this.LaunchBTNtextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.LaunchBTNtextBox.Location = new System.Drawing.Point(123, 218);
            this.LaunchBTNtextBox.Margin = new System.Windows.Forms.Padding(4);
            this.LaunchBTNtextBox.Name = "LaunchBTNtextBox";
            this.LaunchBTNtextBox.ReadOnly = true;
            this.LaunchBTNtextBox.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.LaunchBTNtextBox.Size = new System.Drawing.Size(242, 22);
            this.LaunchBTNtextBox.TabIndex = 9;
            this.LaunchBTNtextBox.Text = "Vybrat";
            this.LaunchBTNtextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.LaunchBTNtextBox.Click += new System.EventHandler(this.LaunchBTNtextBox_Click);
            // 
            // PlayBTN
            // 
            this.PlayBTN.Location = new System.Drawing.Point(53, 466);
            this.PlayBTN.Margin = new System.Windows.Forms.Padding(4);
            this.PlayBTN.Name = "PlayBTN";
            this.PlayBTN.Size = new System.Drawing.Size(282, 28);
            this.PlayBTN.TabIndex = 10;
            this.PlayBTN.Text = "Přehrát / Zastavit";
            this.PlayBTN.UseVisualStyleBackColor = true;
            this.PlayBTN.Click += new System.EventHandler(this.PlayBTN_Click);
            // 
            // DoneBTN
            // 
            this.DoneBTN.Location = new System.Drawing.Point(16, 430);
            this.DoneBTN.Margin = new System.Windows.Forms.Padding(4);
            this.DoneBTN.Name = "DoneBTN";
            this.DoneBTN.Size = new System.Drawing.Size(140, 28);
            this.DoneBTN.TabIndex = 11;
            this.DoneBTN.Text = "Hotovo";
            this.DoneBTN.UseVisualStyleBackColor = true;
            this.DoneBTN.Click += new System.EventHandler(this.DoneBTN_Click);
            // 
            // CancelBTN
            // 
            this.CancelBTN.Location = new System.Drawing.Point(234, 430);
            this.CancelBTN.Margin = new System.Windows.Forms.Padding(4);
            this.CancelBTN.Name = "CancelBTN";
            this.CancelBTN.Size = new System.Drawing.Size(131, 28);
            this.CancelBTN.TabIndex = 12;
            this.CancelBTN.Text = "Zrušit";
            this.CancelBTN.UseVisualStyleBackColor = true;
            this.CancelBTN.Click += new System.EventHandler(this.CancelBTN_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.Filter = "Music files|*.mp3";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label2);
            this.panel1.Location = new System.Drawing.Point(53, 94);
            this.panel1.Margin = new System.Windows.Forms.Padding(4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(267, 123);
            this.panel1.TabIndex = 13;
            // 
            // label2
            // 
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Enabled = false;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(267, 123);
            this.label2.TabIndex = 0;
            this.label2.Text = "Stiskněte jakékoli tlačítko na mřížce launchpadu\r\nESC pro zrušení";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label2.Visible = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(16, 11);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(109, 17);
            this.label6.TabIndex = 14;
            this.label6.Text = "Název záznamu";
            // 
            // NameTextBox
            // 
            this.NameTextBox.Location = new System.Drawing.Point(16, 31);
            this.NameTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.NameTextBox.Name = "NameTextBox";
            this.NameTextBox.Size = new System.Drawing.Size(348, 22);
            this.NameTextBox.TabIndex = 15;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(18, 247);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(61, 17);
            this.label7.TabIndex = 17;
            this.label7.Text = "Červená";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(17, 309);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(52, 17);
            this.label8.TabIndex = 19;
            this.label8.Text = "Zelená";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(18, 373);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(34, 17);
            this.label9.TabIndex = 20;
            this.label9.Text = "List:";
            // 
            // RedTrack
            // 
            this.RedTrack.LargeChange = 4;
            this.RedTrack.Location = new System.Drawing.Point(123, 247);
            this.RedTrack.Maximum = 3;
            this.RedTrack.Name = "RedTrack";
            this.RedTrack.Size = new System.Drawing.Size(160, 56);
            this.RedTrack.TabIndex = 22;
            this.RedTrack.Scroll += new System.EventHandler(this.ColorTrack_Scroll);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(253, 276);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(30, 17);
            this.label10.TabIndex = 23;
            this.label10.Text = "Full";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(129, 276);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(27, 17);
            this.label11.TabIndex = 24;
            this.label11.Text = "Off";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(129, 338);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(27, 17);
            this.label12.TabIndex = 27;
            this.label12.Text = "Off";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(253, 338);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(30, 17);
            this.label13.TabIndex = 26;
            this.label13.Text = "Full";
            // 
            // GreenTrack
            // 
            this.GreenTrack.LargeChange = 4;
            this.GreenTrack.Location = new System.Drawing.Point(123, 309);
            this.GreenTrack.Maximum = 3;
            this.GreenTrack.Name = "GreenTrack";
            this.GreenTrack.Size = new System.Drawing.Size(160, 56);
            this.GreenTrack.TabIndex = 25;
            this.GreenTrack.Scroll += new System.EventHandler(this.ColorTrack_Scroll);
            // 
            // ListNumericUpDown
            // 
            this.ListNumericUpDown.Location = new System.Drawing.Point(123, 373);
            this.ListNumericUpDown.Maximum = new decimal(new int[] {
            8,
            0,
            0,
            0});
            this.ListNumericUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.ListNumericUpDown.Name = "ListNumericUpDown";
            this.ListNumericUpDown.Size = new System.Drawing.Size(160, 22);
            this.ListNumericUpDown.TabIndex = 28;
            this.ListNumericUpDown.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.ListNumericUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // addForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(391, 507);
            this.Controls.Add(this.ListNumericUpDown);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.GreenTrack);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.RedTrack);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.NameTextBox);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.CancelBTN);
            this.Controls.Add(this.DoneBTN);
            this.Controls.Add(this.PlayBTN);
            this.Controls.Add(this.LaunchBTNtextBox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.VolumeTrackBall);
            this.Controls.Add(this.VolumeLabel);
            this.Controls.Add(this.FileTextBox);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "addForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "addForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.addForm_FormClosing);
            this.Load += new System.EventHandler(this.AddForm_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.addForm_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.VolumeTrackBall)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.RedTrack)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GreenTrack)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ListNumericUpDown)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox FileTextBox;
        private System.Windows.Forms.Label VolumeLabel;
        private System.Windows.Forms.TrackBar VolumeTrackBall;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox LaunchBTNtextBox;
        private System.Windows.Forms.Button PlayBTN;
        private System.Windows.Forms.Button DoneBTN;
        private System.Windows.Forms.Button CancelBTN;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox NameTextBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TrackBar RedTrack;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TrackBar GreenTrack;
        private System.Windows.Forms.NumericUpDown ListNumericUpDown;
    }
}