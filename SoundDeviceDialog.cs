﻿using NAudio.Wave;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace SoundBoard
{
    public partial class SoundDeviceDialog : Form
    {

        Guid SpeakGuid;
        Guid MicGuid;

        Form1 form1;

        public SoundDeviceDialog(Form1 f, Guid speak, Guid mic)
        {
            InitializeComponent();
            this.form1 = f;

            SpeakGuid = speak;
            MicGuid = mic;
        }

        private void SoundDeviceDialog_Load(object sender, EventArgs e)
        {

            reflist();

        }

        void reflist()
        {
            

            comboBox1.Items.Clear();
            comboBox2.Items.Clear();

            foreach (var dv in DirectSoundOut.Devices)
            {
                Form1.direcddevice d = new Form1.direcddevice(dv);
                

                comboBox1.Items.Add(d);
                comboBox2.Items.Add(d);

                if (d.device.Guid == SpeakGuid)
                {
                    

                    comboBox1.SelectedIndex = comboBox1.Items.IndexOf(d);
                }
                if (d.device.Guid == MicGuid)
                {
                    

                    comboBox2.SelectedIndex = comboBox2.Items.IndexOf(d);
                }
            }
        }

        

        

        

        private void Submit()
        {
            /*
            if(PrimTextBox.Text == "" || PrimTextBox.Tag == null)
            {
                MessageBox.Show("Je třeba vybrat primární zvukové výstupní zařízení!", "Zařízení nevybráno", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (SecTextBox.Text == "" || SecTextBox.Tag == null)
            {
                MessageBox.Show("Je třeba vybrat sekundární zvukové výstupní zařízení!", "Zařízení nevybráno", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            form1.SetSoundDevice(((Form1.direcddevice)PrimTextBox.Tag).device.Guid, ((Form1.direcddevice)SecTextBox.Tag).device.Guid);*/
            form1.SetSoundDevice(((Form1.direcddevice)comboBox1.SelectedItem).device.Guid, ((Form1.direcddevice)comboBox2.SelectedItem).device.Guid);

            this.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Submit();
            
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void comboBox_DropDown(object sender, EventArgs e)
        {
            reflist();

        }
    }
}
