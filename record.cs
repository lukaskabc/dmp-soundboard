﻿using IntelOrca.Launchpad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoundBoard
{
    public class record
    {

        public record(string fname, int vol, string name, int l, int[] btn, ButtonBrightness r = ButtonBrightness.Full, ButtonBrightness g = ButtonBrightness.Full)
        {
            FileName = fname;
            Volume = vol;
            LaunchButton[0] = btn[0];
            LaunchButton[1] = btn[1];
            Name = name;
            list = l;
            red = r;
            green = g;
        }

        public string FileName;
        public int Volume;
        public int[] LaunchButton = new int[2];
        public string Name;
        public int list;
        public ButtonBrightness red = ButtonBrightness.Full;
        public ButtonBrightness green = ButtonBrightness.Full;

        public override string ToString()
        {
            return Name + " | " + "X: "+LaunchButton[0]+"  /  Y: "+LaunchButton[1];
        }
    }
}
