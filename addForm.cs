﻿using IntelOrca.Launchpad;
using NAudio.Wave;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SoundBoard
{
    public partial class addForm : Form
    {
        Form1 Form1;

        DirectSoundOut Playback;

        private int[] launchkey = new int[2];
        public addForm(Form1 f, int l)
        {
            InitializeComponent();
            this.Text = "Přidat nový záznam";

            VolumeLabel.Text = "Hlasitost (" + VolumeTrackBall.Value.ToString() + "%)";
            this.Form1 = f;
            //f.Hide();
            panel1.Visible = false;
            panel1.Enabled = false;

            ListNumericUpDown.Value = l + 1;

            Playback = new DirectSoundOut(Form1.GetSpeakGUID());
        }
        bool launchkeySeted = false;
        public void LaunchButtonCapture(int padX, int padY, int l)
        {
            if(launchkeySeted) this.Form1.setColor(launchkey[0], launchkey[1], (ButtonBrightness)0, (ButtonBrightness)0);
            waitingOnKey = false;
            launchkey[0] = padX;
            launchkey[1] = padY;
            ListNumericUpDown.Value = l+1;
            launchkeySeted = true;

            this.Form1.setColor(launchkey[0], launchkey[1], (ButtonBrightness)this.red, (ButtonBrightness)this.green);


            if (LaunchBTNtextBox.InvokeRequired)
            LaunchBTNtextBox.Invoke((MethodInvoker)delegate
            {
                LaunchBTNtextBox.Text = "X: " + launchkey[0].ToString() + "  |  Y: " + launchkey[1].ToString();
            });
            else
                LaunchBTNtextBox.Text = "X: " + launchkey[0].ToString() + "  |  Y: " + launchkey[1].ToString();
            // for some reason works now



        }

        private void FileTextBox_Click(object sender, EventArgs e)
        {
            openFileDialog1.FileName = "";
            openFileDialog1.CheckFileExists = true;
            openFileDialog1.CheckPathExists = true;
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                FileTextBox.Text = openFileDialog1.FileName;
                FileTextBox.TextAlign = HorizontalAlignment.Right;
                dataediting = false;
            }
        }

        private void CancelBTN_Click(object sender, EventArgs e)
        {
            this.Form1.Show();
            //this.Form1.AddNewCancel();
            this.Close();
        }

        private void VolumeTrackBall_Scroll(object sender, EventArgs e)
        {
            VolumeLabel.Text = "Hlasitost ("+VolumeTrackBall.Value.ToString()+"%)";
            if(a != null)
                a.Volume = VolumeTrackBall.Value / 100.0f;

            //player.settings.volume = VolumeTrackBall.Value;
        }

        bool waitingOnKey = false;

        private void LaunchBTNtextBox_Click(object sender, EventArgs e)
        {

            if (!Form1.LaunchpadConnected)
            {
                MessageBox.Show("Zařízení není dostupné.", "Device Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }


            waitingOnKey = true;
            panel1.Enabled = true;
            panel1.Visible = true;
            label2.Visible = true;
            label2.Enabled = true;
            this.Enabled = false;

            Form1.addNewActive = true;

            while (waitingOnKey) Application.DoEvents();


            this.Enabled = true;
            panel1.Visible = false;
            panel1.Enabled = false;
            label2.Visible = false;
            label2.Enabled = false;
            Form1.addNewActive = false;


            /*this.Form1.setColor(launchkey[0], launchkey[1], red, green);
            LaunchBTNtextBox.Text = "X: " + launchkey[0].ToString() + "  |  Y: " + launchkey[1].ToString();*/
        }


        AudioFileReader a;
        private void PlayBTN_Click(object sender, EventArgs e)
        {
            if (FileTextBox.Text != "Vybrat")
            {

                if(Playback.PlaybackState != PlaybackState.Playing) {
                    Playback = new DirectSoundOut(Form1.GetSpeakGUID());

                    a = new AudioFileReader(FileTextBox.Text);
                    a.Volume = VolumeTrackBall.Value / 100.0f;

                    Playback.Init(a);
                    Playback.Play();


                } else
                {
                    Playback.Stop();
                }
                /*
                if(player.playState != WMPLib.WMPPlayState.wmppsPlaying)
                {
                    player.settings.volume = VolumeTrackBall.Value;
                    player.URL = FileTextBox.Text;
                    player.controls.play();
                } else
                {
                    player.controls.stop();
                }
                    */


            }
        }

        ButtonBrightness red = ButtonBrightness.Full;
        ButtonBrightness green = ButtonBrightness.Full;

        bool aded = false;

        private void DoneBTN_Click(object sender, EventArgs e)
        {
            if (FileTextBox.Text == "Vybrat" || launchkeySeted == false || NameTextBox.Text.Trim() == "")
            {
                //MessageBox.Show("Please select an audio file, Launchpad button and sample name!");
                MessageBox.Show("Prosím vyberte zvukový soubor, tlačítko launchpadu a název záznamu!");
                return;
            }

            if (!dataediting)
            {
                long time = ((DateTimeOffset)DateTime.UtcNow).ToUnixTimeSeconds();

                string subPath = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "LaunchpadSoundBoard\\WorkingConfiguration");

                if (!System.IO.Directory.Exists(subPath)) System.IO.Directory.CreateDirectory(subPath);


                System.IO.File.Copy(FileTextBox.Text, subPath + "\\" + time.ToString() + NameTextBox.Text.Trim() + ".mp3");

                FileTextBox.Text = subPath + "\\" + time.ToString() + NameTextBox.Text.Trim() + ".mp3";
            }
            record r = new record(FileTextBox.Text.Trim(), VolumeTrackBall.Value, NameTextBox.Text.Trim(), Convert.ToInt32(ListNumericUpDown.Value) - 1, launchkey, red, green);

            aded = true;
            this.Form1.AddInterface(r);
            this.Form1.Show();
            this.Close();

        }

        public void LoadEditData(record r/*string n, string fn, ButtonBrightness r, ButtonBrightness g, int[] buttons, int vol, int l*/)
        {
            this.Text = "Upravit záznam";

            this.NameTextBox.Text = r.Name;
            this.FileTextBox.Text = r.FileName;
            this.red = r.red;
            this.green = r.green;
            this.launchkey = r.LaunchButton;
            this.VolumeTrackBall.Value = r.Volume;
            this.dataediting = true;
            this.launchkeySeted = true;
            waitingOnKey = false;

            ListNumericUpDown.Value = r.list+1;

            VolumeLabel.Text = "Hlasitost (" + VolumeTrackBall.Value.ToString() + "%)";

            LaunchBTNtextBox.Text = "X: " + launchkey[0].ToString() + "  |  Y: " + launchkey[1].ToString();
            RedTrack.Value = (int)this.red;
            GreenTrack.Value = (int)this.green;
        }
        bool dataediting = false;
        /*
        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (waitingOnKey) return;
            string sred = comboBox1.SelectedItem.ToString();
            string sgreen = comboBox2.SelectedItem.ToString();

            switch (sgreen)
            {
                case "Off":
                    green = ButtonBrightness.Off;
                    break;
                case "Low":
                    green = ButtonBrightness.Low;
                    break;
                case "Medium":
                    green = ButtonBrightness.Medium;
                    break;
                case "Full":
                    green = ButtonBrightness.Full;
                    break;
                default:
                    green = ButtonBrightness.Full;
                    break;
            }
            switch (sred)
            {
                case "Off":
                    red = ButtonBrightness.Off;
                    break;
                case "Low":
                    red = ButtonBrightness.Low;
                    break;
                case "Medium":
                    red = ButtonBrightness.Medium;
                    break;
                case "Full":
                    red = ButtonBrightness.Full;
                    break;
                default:
                    red = ButtonBrightness.Full;
                    break;
            }

            this.Form1.setColor(launchkey[0], launchkey[1], red, green);
        }
        */

        private void addForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            //player.controls.stop();
            Playback.Stop();
            Playback.Dispose();
            //this.Form1.Show();
            if(!aded) this.Form1.AddNewCancel(true);
            //this.Close();
            
        }

        private void AddForm_Load(object sender, EventArgs e)
        {

        }

        private void ColorTrack_Scroll(object sender, EventArgs e)
        {
            if (waitingOnKey) return;

            this.red = (ButtonBrightness)RedTrack.Value;
            this.green = (ButtonBrightness)GreenTrack.Value;


            if(launchkeySeted)
                this.Form1.setColor(launchkey[0], launchkey[1], red, green);

        }

        private void addForm_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Escape)
            {
                waitingOnKey = false;
            }
        }
    }
}
