# SoundBoard
Novation Launchpad mini

**DMP 2020/2021**

**Vedoucí práce:**\
Ing. Bc. David Kubát

**Cíl práce:**\
Vytvořte program pro obsluhu MIDI kontroléru Novation Launchpad Mini. 

## Stručný popis jednotlivých kroků vedoucí k dosažení cíle:
1. Vytvoření uživatelské rozhraní pro přehled nastavených zvuků, přidání a úpravu zvuků.
2. Rozpoznání připojeného MIDI kontroléru a upozornění v případě jeho nerozpoznání
3. Ukládání aktuální konfigurace a její načítání (zálohování nastavených MP3 souborů do adresáře)
4. Možnosti pro nastavení zvuků ve formátu MP3 na určitá tlačítka MIDI kontroléru, nastavení barvy tlačítka MIDI kontroléru, nastavení 2 výstupních zvukových adaptérů, přepínání listů v rámci jedné konfigurace, nastavení hlasitosti jednotlivých zvuků
5. Indikace aktuálně zvoleného / přehrávaného zvuku
6. Indikaci pozastaveného zvuku
7. Ovládací tlačítka pro přehrání zvuku do primárního výstupního adaptéru, přehrání zvuku do obou výstupních adaptérů, pozastavení přehrávaného zvuku, znovuspuštění pozastaveného zvuku, asynchronní přehrání zvuků (povolená polyfonie), zapnutí přímého přehrávání do obou zvukových adaptérů

## Kritéria hodnocení:

- Funkčnost výsledné aplikace
- Funkčnost hardwarového zařízení
- Přívětivost uživatelského rozhraní
- Dokumentace
- Obhajoba práce a prezentace – věcnost, správnost a logičnost 